## ntx4-get
ntx4-get was tested on Windows, Ubuntu and OS X, but should also work on other linux distros having bash and perl
###Usage: `ntx4-get $verb $pattern $filter`
* $verb:
    - install: get latest version matching $pattern filtered by $filter
    - search: list all versions matching $pattern filtered by $filter
    - latest: print latest version matching $pattern filtered by $filter
* $pattern: (default: '\*'), allows wildcards (\*?)
* $filter: (default: '.*') filter versions via regular expresion

###Examples:
* `ntx4-get latest` - prints latest available version
* `ntx4-get search 0.12.*-stable` - prints all hotfixes of 0.12 stable series
* `ntx4-get install *-alpha` - installs latest alpha release

###Installation:
#### On Windows
1. Install VS 2013 redistributables [vcredist_x64.exe](https://www.microsoft.com/en-us/download/details.aspx?id=40784)
2. Install [Git for Windows](https://git-scm.com/download/win) 
3. Create directory $DIR, go in and press shift + right mouse button, select open git bash here
4. Run `curl -fkL https://bitbucket.org/nanotrix/ntx4-docs/downloads/ntx4-get-0.0.3-win64.tgz | tar -zxvf -`
5. Type `exit`
6. Open command prompt in $DIR (shift+right mouse, select open command prompt)
5. Add directory to your system path via command `install` 
6. Run `ntx4-get latest *-stable`
7. Set ntx4 version via command `setx NTX4_VERSION $VERSION`
8. Close command prompt
6. Open new command prompt anywhere
7. run `ntx4`

#### On Linux
1. Install [curl](https://curl.haxx.se/), `apt-get install curl`
2. Install [docker](https://docs.docker.com/engine/installation/)
3. Create a temporary directory and cd into it 
4. Run `curl -fkL https://bitbucket.org/nanotrix/ntx4-docs/downloads/ntx4-get-0.0.3-docker.tgz | tar -zxvf -`
5. Run `sudo ./install`
6. Delete temporary directory

#### On Mac OS X
1. Install [docker](https://docs.docker.com/mac/step_one/)
2. Click the icon to launch a Docker Quickstart Terminal window
3. Create a temporary directory and open it
4. Run `curl -fkL https://bitbucket.org/nanotrix/ntx4-docs/downloads/ntx4-get-0.0.3-docker.tgz | tar -zxvf -`
5. Run `sudo ./install`
6. Delete temporary directory

