## ntx4 and ipfs integration
ntx4 can be integrated with [ipfs](https://ipfs.io/) to enable peer2peer sharing of data and configurations 

###Installation:
Either follow official [instructions](https://ipfs.io/docs/install/) or use [docker version](https://hub.docker.com/r/jbenet/go-ipfs/) (recommended)
####Notes on Docker version
1. Create a container keeping data: `docker create --name ipfs_data jbenet/go-ipfs:release /bin/true`
2. Start a service:  `docker run -p 4001:4001 -d --restart=unless-stopped --name ntx4_ipfs_service --volumes-from ipfs_data jbenet/go-ipfs:release`
    - if you want to become public gateway add: -p 8080:8080
    - if you want to access gui and/or API from localhost add: -p 127.0.0.1:5001:5001 
3. Update start-up configuration:
    * remove default bootstrap list: `docker exec ntx4_ipfs_service ipfs bootstrap rm --all`   
    * add this one: `docker exec ntx4_ipfs_service ipfs bootstrap add /ip4/87.236.195.93/tcp/4001/ipfs/QmXCY8BjPFjAmijHgK1VQk4Xdz95E8JtXvb1QaGvfeFCJb`
    * and this one: `docker exec ntx4_ipfs_service ipfs bootstrap add /ip4/147.230.21.56/tcp/4001/ipfs/QmfYdFZcYP7rrNHFmzPfqsZVKFgyMaVWes7jrQoHo8PzgG`
    * and this one: `docker exec ntx4_ipfs_service ipfs bootstrap add /ip4/147.230.77.28/tcp/4001/ipfs/QmXamc7YZAbx6GVeFtj9qKveJaioMgaKZRBrS64jFdxHxx`
    * restart: `docker restart ntx4_ipfs_service`
4. Edit /usr/local/etc/default/ntx4/ntx4.docker and add following lines:
    * --link ntx4_ipfs_service:ntx4_ipfs_service
    * -e ntx4_resources.pullserver=http://ntx4_ipfs_service:8080/ipfs/
    * -e ntx4_resources.pushserver=http://ntx4_ipfs_service:5001/api/v0/

####Notes on Windows version
1. Either create file ntx4.cnf in the ntx4 home folder or cwd and add following lines:
    * resources.pullserver=http://localhost:8080/ipfs/
    * resources.pushserver=http://localhost:5001/api/v0/
2. or set environment variables:
    * setx ntx4_resources.pullserver http://localhost:8080/ipfs/
    * setx ntx4_resources.pushserver http://localhost:5001/api/v0/
3. Update start-up configuration as in case of using docker
4. To start ipfs daemon automatically, use e.g. Windows Task Scheduler via Gui /Accessories->System Tools/ or [cmd](https://technet.microsoft.com/en-us/library/cc772785(v=ws.10).aspx)

  


